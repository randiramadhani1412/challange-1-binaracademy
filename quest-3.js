const checkEmail = (email) => {
  if (typeof email === "undefined") {
    return "Parameter harus diisi";
  } else if (typeof email === "number") {
    return email;
  } else if (/^[a-z0-9]+@[a-z.-]+\.[a-z]{2,3}$/gi.test(email)) {
    return "VALID";
  } else if (/^[a-z0-9]+@[a-z]/gi.test(email)) {
    return "INVALID";
  } else {
    return "Format email tidak valid, contoh : test@mail.com";
  }
};
const checkTypeNumber = (call) => {
  const result = call;
  return result + " ini adalah angka dari parameter fungsi checkEmail";
};
console.log(checkEmail("apranata@binar.com.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkTypeNumber(checkEmail(1232)));
console.log(checkEmail());
