// const checkTypeNumber = (givenNumber) => {
//   let result;
//   if (typeof givenNumber == "number") {
//     result = givenNumber % 2 === 0 ? "GENAP" : "GANJIL";
//   } else if (Array.isArray(givenNumber)) {
//     return "Error : karna parameter adalah array";
//   } else if (typeof givenNumber == "object") {
//     return "Error : karna parameter adalah object";
//   } else {
//     result = givenNumber === undefined ? "Error: Bro where is the parameter ?" : "Error: Invalid data type";
//   }
//   return result;
// };
const checkTypeNumber = (givenNumber) => {
  let result;
  if (typeof givenNumber == "number") {
    result = givenNumber % 2 === 0 ? "GENAP" : "GANJIL";
  } else {
    result = givenNumber === undefined ? "Error: Bro where is the parameter ?" : "Error: Invalid data type";
  }
  return result;
};
console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("10"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());
